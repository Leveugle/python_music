from algos.mix_unique_tracks import create_dict_event_n_previous, get_full_list_events
from work_on_midi.utils import create_track, get_average_len_of_list_of_notes


def create_solo_track(starting_note, starting_time, list_of_list_notes, list_of_list_time, velocity, instrument_number):
    print("Get number of notes")
    number_of_notes = get_average_len_of_list_of_notes(list_of_list_notes)

    print("Get final list of notes")
    nb_previous_note = len(starting_note)
    dict_transition_notes = create_dict_event_n_previous(list_of_list_notes, nb_previous_note)

    final_list_notes = get_full_list_events(starting_note, number_of_notes, dict_transition_notes)

    print("Get list of time")
    nb_previous_time = len(starting_time)
    dict_transition_times = create_dict_event_n_previous(list_of_list_time, nb_previous_time)

    final_list_times = get_full_list_events(starting_time, number_of_notes, dict_transition_times)

    print("Create track")
    track_created = create_track(final_list_notes, final_list_times, velocity=velocity,
                                 instrument_number=instrument_number)
    print("Track Created")
    return track_created
