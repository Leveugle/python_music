import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage
import os
import numpy as np
import copy
from collections import Counter
import random
import json
import pickle
from work_on_midi.utils import get_octave, get_original_note


def read_files_folder(folder, list_names):
    """
    Read all midi files from the list and return a list of midi files
    """
    list_files = []
    for name in list_names:
        midi_file = MidiFile(os.path.join(os.getcwd(), folder, name), clip=True)
        list_files.append(midi_file)
    return list_files


def get_longest_track(midi_file):
    """
    Return the longest track from a midi file
    """
    list_of_lenghts = [len(tracks) for tracks in midi_file.tracks]
    return list_of_lenghts.index(max(list_of_lenghts))


def get_list_of_longest_tracks_from_list_of_midi(list_of_midi_files):
    """
    Get list of tracks from a list of midi files
    """

    list_of_tracks = []
    for midi_file in list_of_midi_files:
        list_of_tracks.append(midi_file.tracks[get_longest_track(midi_file)])
    return list_of_tracks


def get_list_of_tracks_from_list_of_midi(list_of_midi_files, min_length=200):
    """
    Get list of tracks from a list of midi files
    """

    list_of_tracks = []
    for midi_file in list_of_midi_files:
        for track in midi_file.tracks:
            if len(track) >= min_length:
                list_of_tracks.append(track)
    return list_of_tracks


def get_infos_from_track_no_null_velocity(track):
    """
    List all notes, time and octaves from a list  of track. Remove velocity of 0
    """
    instrument = 1
    for i in range(20):
        if track[i].type == "program_change":
            instrument = track[i].program
    list_notes = []
    list_octave = []
    list_time = []
    for message in track:
        try:
            if message.velocity != 0:
                list_octave.append(get_octave(message.note))
                list_notes.append(get_original_note(message.note))
                try:
                    list_time.append(previous_time + message.time)
                except Exception as e:
                    list_time.append(message.time)
            previous_time = message.time
        except Exception as e:
            pass
    return list_octave, list_notes, list_time, instrument

def get_infos_from_track(track):
    """
    List all notes, time and octaves from a list  of track
    """
    instrument = 1
    for i in range(20):
        if track[i].type == "program_change":
            instrument = track[i].program
    list_notes = []
    list_octave = []
    list_time = []
    for message in track:
        try:
            list_octave.append(get_octave(message.note))
            list_notes.append(get_original_note(message.note))
            try:
                list_time.append(message.time)
            except Exception as e:
                list_time.append(0)
        except Exception as e:
            pass
    return list_octave, list_notes, list_time, instrument


def get_infos_from_list_of_tracks(list_of_tracks):
    """
    List of lists for notes octaves and time, from a list of tracks
    """
    list_of_list_notes = []
    list_of_list_octave = []
    list_of_list_time = []
    list_of_instruments = []
    for track in list_of_tracks:
        list_octave, list_notes, list_time, instrument = get_infos_from_track(track)
        list_of_list_notes.append(list_notes)
        list_of_list_octave.append(list_octave)
        list_of_list_time.append(list_time)
        list_of_instruments.append(instrument)
    return list_of_list_octave, list_of_list_notes, list_of_list_time, list_of_instruments


def get_octaves_count(list_of_list_of_octaves):
    """
    From a list of list of octaves get the appearance of octaves
    """
    dict_octaves = {}
    for i in range(-1, 11):
        dict_octaves[i] = 0

    for list_of_octaves in list_of_list_of_octaves:
        count_octaves = Counter(list_of_octaves).most_common()
        nb_octaves = len(count_octaves)

        for i in range(nb_octaves):
            dict_octaves[count_octaves[i][0]] += count_octaves[i][1]

    return dict_octaves


def get_best_two_octaves(octave_count):
    """
    from a dictionnary of octaves, get the best 2 octaves
    Will return the minimum of the 2 octaves, the other will be minimum +1
    """
    octave_minimum = sorted(octave_count.items(), key=lambda x: x[1], reverse=True)[0][0]
    try:
        # if we have at least 2 octaves, if the second is lower, we remove 1 to the min, else we let like this.
        # In the second case, the 2nd octave will be considered to be minimum + 1 (in the other functions)
        second_octave = sorted(octave_count.items(), key=lambda x: x[1], reverse=True)[1][0]
        if (second_octave < octave_minimum):
            octave_minimum = octave_minimum - 1
    except Exception as e:
        pass
    return octave_minimum


def get_base_time(list_of_time):
    """
    From a list of time, get the base time, a unit from where all other times can be computed
    """
    base_time = 64
    total_times = len(list_of_time)

    count_times = Counter(list_of_time)
    sorted_counted_times = sorted(count_times.items(), key=lambda x: x[1], reverse=True)

    # Getting list of recurrent times
    list_base_times = []
    threshold = 0.05
    for i in range(len(sorted_counted_times)):
        if sorted_counted_times[i][1] / total_times >= threshold:
            list_base_times.append(sorted_counted_times[i][0])

    # if no time more than threshold, take the one that appear the most
    if len(list_base_times) == 0:
        base_time = sorted_counted_times[0][0]
    # If just 1 time, take it
    elif len(list_base_times) == 1:
        base_time = list_base_times[0]
    # IF more than 1 time, check if the ratio is an integer. If yes take the minimum, if not take maximum of 2
    else:
        list_base_times.sort()
        if (list_base_times[1] / list_base_times[0]).is_integer():
            base_time = list_base_times[0]
        else:
            base_time = list_base_times[1]

    return base_time


def get_list_time_from_base(list_time, base_time):
    """
    divide all times of a list by the base time to get a list of times depending of the base time
    """
    list_based = [time / base_time for time in list_time]
    return list_based


"""
music_input_folder = "C:\\Users\\sylva\\Documents\\python_music\\music_input\\Brahms"
list_of_files = ["brahms_cello_sonata_38.mid"]
print("Reading files")
list_midi_files = read_files_folder(music_input_folder, list_of_files)

print("Get list of tracks")
list_of_tracks = get_list_of_tracks_from_list_of_midi(list_midi_files)

print("Getting infos")

list_list_octave, list_list_notes, list_list_time, list_instrument = get_infos_from_list_of_tracks(list_of_tracks)
print("End")
"""