# Target format of a music :
music = {
    "params": {},
    "music": [
        {
            "notes": [64, 67, 69],
            "instruments": [1, 42, 1],  # Not confirmed
            "notes_instru": [{64: 1, 67: 42, 69: 1}], # Not confirmed
            "time": 120
        }
    ]
}

'''
Morceau piano violoncelle
2 instruments, le piano peut faire plein de notes d'un coup.
On définit qu'on a 3 tracks (violoncelle, piano main droite et piano main gauche)
Potentiellement à adapter : il faut être capable d'identifier main droite et gauche dans le fichier midi

On doit avoir au moins 3 tracks en entrée (1 pour chaque instrument)
On construit la musique qui contiendra à chauqe fois au moins 3 notes à la fois

On génère notre musique qui contiendra pile 3 notes à chaque événement

A posteriori : 
Les 'plusieurs notes d'un coup' (accords) sont listées. 
A chaque fois qu'une note est choisie pour l'instrument, 
si elle peut générer un accord, on génère l'accord selon une proba

TODO : 
- Récupérer un morceau, isoler les 3 parties qu'on veut
- Créer la liste des accords pour chaque instrument 
- Créer la liste des events comme le format ci-dessus
- Faire la matrice de transition correpsondante (3 notes d'un coup pour les 3 tracks, 2 dernière notes prises en compte)
- Prendre les 2 premières notes, et appliquer la matrice de transition
- Adapter à n morceaux
'''
