import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage
import os
import numpy as np
import copy
from collections import Counter
import random
import json
import pickle


def get_original_note(x):
    """
    Return the note from 0 to 12 corresponding to the note
    input : A note number from a midi file, between 0 and 127
    """
    return x % 12


def get_octave(x):
    """
    Return the octave corresponding to the note
    input : A note number from a midi file, between 0 and 127
    """
    return x // 12


def get_real_note_with_octave(octave, note):
    """
    From the octave and the note, return the real number of the note for the midi file
    """
    return (octave * 12 + note)


def keep_2_octaves(list_octaves, octave_min):
    """
    Keep only 2 consecutive octaves in a list of octaves
    """
    list_octave_modified = []
    for octave in list_octaves:
        if octave <= octave_min:
            list_octave_modified.append(octave_min)
        else:
            list_octave_modified.append(octave_min + 1)
    return list_octave_modified


def change_notes_on_2_octaves(list_notes, list_octaves):
    """Change a list of notes to get real notes in the 2 octaves"""
    list_new_notes = []
    for i in range(len(list_notes)):
        list_new_notes.append(get_real_note_with_octave(list_octaves[i], list_notes[i]))
    return list_new_notes


def create_track(list_notes, list_time, velocity, track_name='generated_track', instrument_name='Instrument',
                 instrument_number=1):
    """
    Use lists of notes and time to create a track
    """
    track = MidiTrack()

    # Track initi
    track.append(mido.MetaMessage('track_name', name=str(track_name), time=0))
    track.append(mido.MetaMessage('instrument_name', name=instrument_name, time=0))
    track.append(mido.Message('program_change', channel=0, program=instrument_number, time=0))

    # In case additional notes have been added and not times
    while len(list_notes) >= len(list_time):
        list_time = list_time + list_time
    # Add notes
    for i in range(len(list_notes) - 1):
        track.append(Message('note_on', note=list_notes[i], velocity=velocity, time=0))
        track.append(Message('note_off', note=list_notes[i], velocity=velocity, time=list_time[i]))

    # End track
    track.append(mido.MetaMessage('end_of_track', time=0))

    return track


def get_min_and_max_notes(list_of_list_of_notes):
    """
    get the minimum and maximum notes from list of list of notes
    """
    minimum = 127
    maximum = 0
    for list_of_notes in list_of_list_of_notes:
        minimum = min(min(list_of_notes), minimum)
        maximum = max(max(list_of_notes), maximum)
    return (minimum, maximum)


def get_average_len_of_list_of_notes(list_of_list_of_notes):
    """
    From a list of list of notes, get the average length
    """
    list_of_len = [len(list_of_notes) for list_of_notes in list_of_list_of_notes]
    average = sum(list_of_len) / len(list_of_len)
    return int(np.floor(average))


def get_cumulated_time(list_of_times):
    """
    From a list times, get a list of cumulative times
    """

    list_of_cumulative_times = []
    for time in list_of_times:
        try:
            list_of_cumulative_times.append(time + list_of_cumulative_times[-1])
        # Exception for the first element where the list is still empty
        except Exception:
            list_of_cumulative_times.append(time)

    return list_of_cumulative_times


def get_not_cumulated_list_time(list_cumulated_times):
    """
    From a list of cumulated_time, give the list of uncumulated times
    ex :  [120,240,360] becomes [120,120,120]
    """
    non_cumulated_list_times =[list_cumulated_times[0]]
    for i in range(1, len(list_cumulated_times)):
        non_cumulated_list_times.append(list_cumulated_times[i] - list_cumulated_times[i - 1])
    return non_cumulated_list_times

def sort_dict(dictionnary):
    """
    sort a simple dictionnary on its values
    """
    return {k: v for k, v in sorted(dictionnary.items(), key=lambda item: item[1])}


def get_matching_tracks(dict_midi_files, main_music_file):
    """
    from a dictionnary of musics, and a main music file, get for each track, the best matching track in other musics
    """
    dict_sorted_tracks = {}
    dict_midi_file_copy = copy.deepcopy(dict_midi_files)
    # sorting tracks by their min octave
    for music_file in dict_midi_files.keys():
        new_dict = {}
        for track_no in dict_midi_files[music_file]["tracks"].keys():
            new_dict[track_no] = dict_midi_files[music_file]["tracks"][track_no]["octave_minimum"]

        # dict_midi_file_copy contain for each music files, tracks numbers ordered by octaves
        dict_sorted_tracks[music_file] = sort_dict(new_dict)

    matching_tracks = {}
    for track_number in dict_midi_files[main_music_file]["tracks"].keys():
        main_instrument = dict_midi_files[main_music_file]["tracks"][track_number]["instrument"]
        matching_tracks[track_number] = {main_music_file: track_number}

        for music_file in dict_sorted_tracks.keys():
            if music_file != main_music_file:
                for track_number_bis in dict_sorted_tracks[music_file].keys():
                    if dict_midi_file_copy[music_file]["tracks"][track_number_bis]["instrument"] == main_instrument:
                        matching_tracks[track_number][music_file] = track_number_bis
                        dict_midi_file_copy[music_file]["tracks"][track_number_bis]["instrument"] = 99
                        break
    return matching_tracks


def create_matrix_notes(list_of_lists_of_notes):
    # Create empty matrix
    min_matrix, max_matrix = get_min_and_max_notes(list_of_lists_of_notes)
    size_matrix = max_matrix - min_matrix + 1
    matrix = np.zeros((size_matrix, size_matrix))
    print("Matrix of size {} created".format(size_matrix))

    # Fill Matrix
    for list_notes in list_of_lists_of_notes:
        for i in range(len(list_notes) - 2):
            # Take note and following note. We remove the min to get the corresponding lines in the matrix
            note = list_notes[i] - min_matrix
            next_note = list_notes[i + 1] - min_matrix

            # Add 1 occurence in the matrix
            matrix[note, next_note] += 1
    return matrix, min_matrix


def clean_list_times(list_times, min_value=0, max_value=200):
    """
    Clean exwtreme values in list of times
    """
    new_list = []
    replacing_value = Counter(list_times).most_common()[0][0]
    for time in list_times:
        if time < min_value or time > max_value:
            new_list.append(replacing_value)
        else:
            new_list.append(time)
    return new_list


def create_new_music(music_input_folder, music_output_folder, list_of_files, output_name, instrument_name,
                     instrument_number, tempo=750000, velocity=100, starting_note=[0], starting_time=[120]):
    """
    Will use all functions to read list of midi files, treat them and export the result
    """
    print("Reading files")
    list_midi_files = read_files_folder(music_input_folder, list_of_files)
    print("Creating first track")
    first_track = MidiTrack([
        MetaMessage('track_name', name=output_name, time=0),
        MetaMessage('smpte_offset', frame_rate=25, hours=32, minutes=0, seconds=3, frames=0, sub_frames=0, time=0),
        MetaMessage('time_signature', numerator=4, denominator=4, clocks_per_click=24, notated_32nd_notes_per_beat=8,
                    time=0),
        MetaMessage('key_signature', key='G', time=0),
        MetaMessage('set_tempo', tempo=tempo, time=0),
        MetaMessage('end_of_track', time=0)])

    print("Get list of tracks")
    list_of_tracks = get_list_of_longest_tracks_from_list_of_midi(list_midi_files)

    print("Get list of infos from tracks")
    list_of_list_octave, list_of_list_notes, list_of_list_time = get_infos_from_list_of_tracks(list_of_tracks)

    print("Getting octaves of work")
    octave_count = get_octaves_count(list_of_list_octave)
    octave_minimum = get_best_two_octaves(octave_count)
    print("Octaves will be {} and {}".format(octave_minimum, octave_minimum + 1))

    print("Updating list of octaves")
    list_of_list_octave_updated = [keep_2_octaves(list_octave, octave_minimum) for list_octave in list_of_list_octave]

    print("Updating list of notes")
    list_of_list_notes_updated = []
    for i in range(len(list_of_list_octave_updated) - 1):
        list_of_list_notes_updated.append(
            change_notes_on_2_octaves(list_of_list_notes[i], list_of_list_octave_updated[i]))

    print("Get number of notes")
    number_of_notes = get_average_len_of_list_of_notes(list_of_list_notes_updated)

    print("Get final list of notes")
    nb_previous_note = len(starting_note)
    dict_transition_notes = create_dict_event_n_previous(list_of_list_notes_updated, nb_previous_note)

    final_list_notes = get_full_list_events(starting_note, number_of_notes, dict_transition_notes)

    print("Get list of time")
    nb_previous_time = len(starting_time)
    dict_transition_times = create_dict_event_n_previous(list_of_list_time, nb_previous_time)

    final_list_times = get_full_list_events(starting_time, number_of_notes, dict_transition_times)

    print("Create track")
    track_created = create_track(final_list_notes, final_list_times, velocity, output_name, instrument_name,
                                 instrument_number)

    print("Create Midi file")
    finale_file = MidiFile()
    finale_file.tracks.append(first_track)
    finale_file.tracks.append(track_created)

    print("Save files")
    finale_file.save(os.path.join(music_output_folder, '{}.mid'.format(output_name)))

    with open(os.path.join(music_output_folder, "dict_transitions",
                           'dict_transition_notes_{}.pickle'.format(output_name)), "wb") as handle:
        pickle.dump(dict_transition_notes, handle, protocol=pickle.HIGHEST_PROTOCOL)
    with open(os.path.join(music_output_folder, "dict_transitions",
                           'dict_transition_times_{}.pickle'.format(output_name)), "wb") as handle:
        pickle.dump(dict_transition_times, handle, protocol=pickle.HIGHEST_PROTOCOL)

    print("Finished ;)")
