import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage
import os
import numpy as np
import copy
from collections import Counter
import random
import json
import pickle
from work_on_midi.reading_files import read_files_folder, get_list_of_tracks_from_list_of_midi, \
    get_infos_from_list_of_tracks, get_octaves_count, get_best_two_octaves
from work_on_midi.utils import keep_2_octaves, change_notes_on_2_octaves, get_matching_tracks, clean_list_times
from work_on_midi.track_creation import create_solo_track
from work_on_midi.mixing import concat_n_tracks_in_1, get_n_list_from_unique_list_of_events, concat_n_list_notes_in_1
from algos.mix_unique_tracks import create_dict_event_n_previous, get_full_list_events
from utils import create_track, get_cumulated_time, get_not_cumulated_list_time, get_average_len_of_list_of_notes
from alter_music.times import change_rythm_music


def create_new_music(music_input_folder, music_output_folder, list_of_files, output_name,
                     tempo=750000, velocity=100, number_starting_note=2, number_starting_time=2):
    """
    Will use all functions to read list of midi files, treat them and export the result
    """

    print("Reading files")
    if ~os.path.isdir(music_input_folder):
        music_input_folder = os.path.join(os.getcwd(), music_input_folder)

    dict_midi_files = {}
    main_file = list_of_files[0]
    for file_name in list_of_files:
        try:
            midi_file = MidiFile(os.path.join(music_input_folder, file_name), clip=True)
            dict_midi_files[file_name] = {"midi_file": midi_file}
        except Exception as e:
            raise e

    print("Creating first track")
    print("Create Midi file")
    finale_file = MidiFile()
    first_track = MidiTrack([
        MetaMessage('track_name', name=output_name, time=0),
        MetaMessage('smpte_offset', frame_rate=25, hours=32, minutes=0, seconds=3, frames=0, sub_frames=0, time=0),
        MetaMessage('time_signature', numerator=4, denominator=4, clocks_per_click=24, notated_32nd_notes_per_beat=8,
                    time=0),
        MetaMessage('key_signature', key='G', time=0),
        MetaMessage('set_tempo', tempo=tempo, time=0),
        MetaMessage('end_of_track', time=0)])
    finale_file.tracks.append(first_track)

    print("Get list of tracks")
    for file_name in dict_midi_files:
        print("Gathering infos on file : {}".format(file_name))
        list_of_tracks = get_list_of_tracks_from_list_of_midi([dict_midi_files[file_name]["midi_file"]], min_length=200)
        dict_midi_files[file_name]["list_of_tracks"] = list_of_tracks
        dict_midi_files[file_name]["number_of_tracks"] = len(list_of_tracks)
        dict_midi_files[file_name]["tracks"] = {}

        print("Get infos from tracks")
        for i in range(dict_midi_files[file_name]["number_of_tracks"]):
            dict_midi_files[file_name]["tracks"][i] = {}
            list_of_list_octave, list_of_list_notes, list_of_list_time, list_of_instruments = \
                get_infos_from_list_of_tracks([list_of_tracks[i]])

            # We applied the get_infos function on 1 track, so we only take the first and only element of each result
            dict_midi_files[file_name]["tracks"][i]["list_of_octaves"] = list_of_list_octave[0]
            dict_midi_files[file_name]["tracks"][i]["list_of_notes"] = list_of_list_notes[0]
            dict_midi_files[file_name]["tracks"][i]["list_of_time"] = list_of_list_time[0]
            dict_midi_files[file_name]["tracks"][i]["instrument"] = max(list_of_instruments[0], 1)

            print("Getting octaves of work for track {}".format(str(i)))
            octave_count = get_octaves_count(list_of_list_octave)
            octave_minimum = get_best_two_octaves(octave_count)

            print("Octaves will be {} and {} for track {}".format(octave_minimum, octave_minimum + 1, str(i)))
            dict_midi_files[file_name]["tracks"][i]["octave_minimum"] = octave_minimum

            print("Updating list of octaves for track {}".format(str(i)))
            list_of_list_octave_updated = [keep_2_octaves(list_octave, octave_minimum) for list_octave in
                                           list_of_list_octave]
            dict_midi_files[file_name]["tracks"][i]["list_of_octaves_updated"] = list_of_list_octave_updated[0]

            print("Updating list of notes for track {}".format(str(i)))
            list_notes_updated = change_notes_on_2_octaves(list_of_list_notes[0], list_of_list_octave_updated[0])
            dict_midi_files[file_name]["tracks"][i]["list_notes_updated"] = list_notes_updated

            print("Cleaning times for track {}".format(str(i)))
            list_times_updated = clean_list_times(list_of_list_time[0], min_value=0, max_value=500)
            dict_midi_files[file_name]["tracks"][i]["list_times_updated"] = list_times_updated
            print('test')

    print("Starting creating new music")
    dict_matching_tracks = get_matching_tracks(dict_midi_files, main_music_file=main_file)

    # get the list of list of events for each music, and concatenate them in 1 list
    list_of_list_concatenated_notes = []
    list_of_list_concatenated_times = []
    for music_name in dict_midi_files.keys():
        list_of_list_notes = []
        list_of_list_cumulated_times = []
        for number_track in dict_matching_tracks.keys():
            good_track_for_music = dict_matching_tracks[number_track][music_name]

            list_of_list_notes.append(dict_midi_files[music_name]['tracks'][good_track_for_music]["list_notes_updated"])

            cumulated_time_temp = get_cumulated_time(
                dict_midi_files[music_name]['tracks'][good_track_for_music]["list_times_updated"])
            list_of_list_cumulated_times.append(cumulated_time_temp)
        concatenated_list_times, concatenated_list_notes = concat_n_tracks_in_1(list_of_list_cumulated_times,
                                                                                list_of_list_notes)

        list_of_list_concatenated_notes.append(concat_n_list_notes_in_1(concatenated_list_notes))
        list_of_list_concatenated_times.append(get_not_cumulated_list_time(concatenated_list_times))

    print("Get number of notes")
    number_of_notes = get_average_len_of_list_of_notes(list_of_list_notes)

    print("Get final list of notes")
    dict_transition_notes = create_dict_event_n_previous(list_of_list_concatenated_notes, number_starting_note)

    starting_notes = []
    for i in range(number_starting_note):
        starting_notes.append(list_of_list_concatenated_notes[0][i])
    final_list_notes = get_full_list_events(starting_notes, number_of_notes, dict_transition_notes)

    print("Get list of time")
    dict_transition_times = create_dict_event_n_previous(list_of_list_concatenated_times, number_starting_time)

    starting_times = []
    for i in range(number_starting_time):
        starting_times.append(list_of_list_concatenated_times[0][i])
    final_list_times = get_full_list_events(starting_times, number_of_notes, dict_transition_times)

    final_list_list_notes = get_n_list_from_unique_list_of_events(final_list_notes)

    dict_midi_files[output_name] = {"tracks": {}}
    for numero_track in dict_midi_files[main_file]["tracks"].keys():
        dict_midi_files[output_name]["tracks"][numero_track] = {
            "instrument": dict_midi_files[main_file]["tracks"][numero_track]["instrument"]}

    for track_number in dict_midi_files[output_name]["tracks"].keys():
        print("Creating track corresponding to track {}".format(str(track_number)))
        instrument = dict_midi_files[main_file]["tracks"][track_number]["instrument"]

        track_created = create_track(final_list_list_notes[track_number], final_list_times, velocity=velocity,
                                     instrument_number=instrument)

        finale_file.tracks.append(track_created)

    # Get slower rythm because of concatenation of times
    finale_file = change_rythm_music(finale_file, 4)
    path_save = os.path.join(music_output_folder, '{}.mid'.format(output_name))
    print("Save file in {}".format(path_save))
    finale_file.save(path_save)

    print("Finished ;)")


music_input_folder = "C:\\Users\\sylva\\Documents\\python_music\\music_input\\Brahms"
list_of_files = ["brahms_cello_sonata_38.mid", "brahms_cello_sonata_99.mid"]
music_output_folder = 'C:\\Users\\sylva\\Documents\\python_music\\music_output'
output_name = 'slower_multi_bramhs_99'
create_new_music(music_input_folder, music_output_folder, list_of_files, output_name,
                 tempo=750000, velocity=100, number_starting_note=2, number_starting_time=2)
