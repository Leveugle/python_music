import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage
import os
import numpy as np
import copy
from collections import Counter
import random
import json
import pickle
from work_on_midi.utils import get_octave, get_original_note
from checks import check_size_lists


def concat_n_tracks_in_1(list_of_list_cumulated_times, list_of_list_notes):
    """
    From n list of times and notes, get a unique list of times, with all notes corresponding
    """
    final_list_times = []

    for list_of_times in list_of_list_cumulated_times:
        final_list_times = final_list_times + list_of_times
    final_list_times = sorted(list(set(final_list_times)))


    final_list_list_notes = list(range(len(list_of_list_notes)))
    for i in range(len(list_of_list_notes)):
        old_list_notes = copy.deepcopy(list_of_list_notes[i])
        old_list_times = copy.deepcopy(list_of_list_cumulated_times[i])
        array_old_list_time = np.array(old_list_times)

        # Initiate the new list with a 0 or the first element
        new_list_notes = []

        # For each time, if the time is in the list of times, add the corresponding note, if not add previous element
        for new_time in final_list_times:

            # get list of index with the same time
            indexes_time = list(np.where(array_old_list_time == new_time)[0])

            # New time not in the old list
            if len(indexes_time) == 0:
                if len(new_list_notes) == 0:
                    new_list_notes.append(0)
                else:
                    new_list_notes.append(new_list_notes[-1])

            # Old time present and only 1 element
            elif len(indexes_time) == 1:
                new_list_notes.append(old_list_notes[indexes_time[0]])

            # More than 1 element
            else:
                # get a sorted list of notes (no duplicates) from the indexes
                new_notes = sorted(list(set((map(old_list_notes.__getitem__, indexes_time)))))
                if len(new_notes) == 1:
                    new_list_notes.append(new_notes[0])
                else:
                    new_list_notes.append(new_notes)

        final_list_list_notes[i] = new_list_notes

    return final_list_times, final_list_list_notes


def concat_n_list_notes_in_1(list_of_list_notes):
    """
    From n list of notes, get a unique list of events, each containing n notes
    ex : [1,2,3] and [4,5,6] becomes [[1,2],[3,4],[5,6]]
    """
    check_size_lists(list_of_list_notes)
    final_list_events = []
    if len(list_of_list_notes) <= 1:
        final_list_events = copy.deepcopy(list_of_list_notes[0])
        for i in range(len(final_list_events)):
            if isinstance(final_list_events[i], (list, tuple)):
                final_list_events[i] = final_list_events[i][0]
    else:
        for i in range(len(list_of_list_notes[0])):
            event_temp = []
            for list_notes in list_of_list_notes:
                # If we have a list of notes, we take the first one (it has been sorted before in theory)
                if isinstance(list_notes[i], (list, tuple)):
                    event_temp.append(list_notes[i][0])
                else:
                    event_temp.append(list_notes[i])
            final_list_events.append(tuple(event_temp))
    return final_list_events


def get_n_list_from_unique_list_of_events(list_of_events):
    """
    From a list of multiple events, get a list of list of notes
    ex :  [[1,2],[3,4],[5,6]]  becomes [1,2,3] and [4,5,6]
    """
    number_list = len(list_of_events[0])
    final_list_of_list = []
    for i in range(number_list):
        final_list_of_list.append([])
    for multiple_events in list_of_events:
        for i in range(number_list):
            try:
                final_list_of_list[i].append(multiple_events[i])
            except Exception:
                pass
    return final_list_of_list

