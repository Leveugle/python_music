import mido
from mido import Message, MidiFile, MidiTrack
import os
import numpy as np
import copy
from collections import Counter
'''
cv1 = MidiFile(os.path.join("music_files", 'VampireKillerCV1.mid'), clip=True)
# Create a new file, just writting the sames notes with piano
mid = MidiFile()

track = MidiTrack()

track.append(mido.MetaMessage('track_name', name='Staff-2', time=0))
track.append(mido.MetaMessage('instrument_name', name='Piano', time=0))
track.append(mido.Message('program_change', channel=10, program=1, time=0))

for i in range(120):
    track.append(Message('note_on', note=64, velocity=i, time=15))
for i in range(120):
    track.append(Message('note_on', note=64, velocity=120-i, time=15))
for i in range(120):
    track.append(Message('note_on', note=64, velocity=i, time=15))
for i in range(120):
    track.append(Message('note_on', note=64, velocity=120-i, time=15))

track.append(Message('note_on', note=64, velocity=0, time=0))

track.append(mido.MetaMessage('end_of_track', time=0))

mid.tracks.append(track)
'''
test = MidiFile("C:\\Users\\sylva\\Documents\\python_music\\music_input\\Brahms\\brahms_cello_sonata_38.mid", clip=True)
out = MidiFile()
out.tracks.append(test.tracks[0])
out.tracks.append(test.tracks[2])
out.save('C:\\Users\\sylva\\Documents\\python_music\\music_output\\test2.mid')
