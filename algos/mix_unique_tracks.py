import copy
import numpy as np


def create_dict_event_n_previous(list_of_lists_of_event, number_previous_events):
    """
    from a list of events (notes or times), create the dictionnary of transitions
    ex of return :
    {
    [7,8]:{7:25,8:14,9:25},  after events 7 and 8, we got 25 event 7, 14 event 8 and 25 event 9
    [8,9]:{7:35,8:24,9:13}
    }
    """

    dict_transition_event = {}

    for list_of_events in list_of_lists_of_event:
        previous_event = list_of_events[0:number_previous_events]
        # Go through all events (notes or time)
        for event in list_of_events[number_previous_events:]:
            previous_event_tuple = tuple(previous_event)
            # Check the (previous_event) is already in the dictionnary
            if previous_event_tuple not in dict_transition_event:
                dict_transition_event[previous_event_tuple] = {}
                dict_transition_event[previous_event_tuple][event] = 1
            else:
                # Check that actual note in the list of possible events
                if event not in dict_transition_event[previous_event_tuple]:
                    dict_transition_event[previous_event_tuple][event] = 1
                else:
                    # Add +1 to occurence
                    dict_transition_event[previous_event_tuple][event] += 1
            previous_event.pop(0)
            previous_event.append(event)
    return dict_transition_event


def create_dict_multi_notes(list_notes, list_times):
    """
    from a list of times and notes, get the list of multi notes, meaning the notes that can be played at the same time
    than the original note (the lowest).
    List of notes and times should have the same length
    ex of return :
    {
    7:{[]:100,[8,9]:25,[9,10,11]:14,[8,10,12]:25},  At the same time that note 7, 100 times there was nothing additional
                                                    25 times notes 8 and 9, 14 times notes 9,10 and 11 ...
    8:{[]:105:[9,11]:25,[9,10,11]:14,[10,12]:25}
    }
    """
    min_length = min(len(list_notes), len(list_times))
    list_notes = list_notes[:min_length]
    list_times = list_times[:min_length]
    previous_note = list_notes[0]
    dict_multi_notes = {}
    additional_notes = []

    for i in range(1, min_length):
        # If time is 0, note is at the same moment so we add to the list of additional notes
        if list_times[i] == 0:
            additional_notes.append(list_notes[i])

        else:
            # We take only notes different from previous_note
            if len(additional_notes) > 0:
                additional_notes = [note for note in additional_notes if (note != previous_note)]
                additional_notes.sort()
            additional_notes_tuple = tuple(additional_notes)

            # Check the (previous_note) is already in the dictionnary
            if previous_note not in dict_multi_notes:
                dict_multi_notes[previous_note] = {}
                dict_multi_notes[previous_note][additional_notes_tuple] = 1

            else:
                # Check that additionnal note list in the list of possible lists
                if additional_notes_tuple not in dict_multi_notes[previous_note]:
                    dict_multi_notes[previous_note][additional_notes_tuple] = 1
                else:
                    dict_multi_notes[previous_note][additional_notes_tuple] += 1

            # Once everything added to dict, we reset values
            previous_note = list_notes[i]
            additional_notes = []

    # Add the last iteration if there is one
    if len(additional_notes) > 0:
        # We take only notes different from previous_note
        if len(additional_notes) > 0:
            additional_notes = [note for note in additional_notes if (note != previous_note)]
            additional_notes.sort()
        additional_notes_tuple = tuple(additional_notes)

        if previous_note not in dict_multi_notes:
            dict_multi_notes[previous_note] = {}
            dict_multi_notes[previous_note][additional_notes_tuple] = 1

        else:
            # Check that additionnal note list in the list of possible lists
            if additional_notes_tuple not in dict_multi_notes[previous_note]:
                dict_multi_notes[previous_note][additional_notes_tuple] = 1
            else:
                dict_multi_notes[previous_note][additional_notes_tuple] += 1

    return dict_multi_notes


def predict_next_event(event, dict_transition_event, first_event):
    """
    Predict the next event using the actual event and the dictionnary of transitions
    """

    try:
        dict_of_possible_events = dict_transition_event[tuple(event)]
        total = sum(dict_of_possible_events.values())
        if total == 0:
            print("WARNING we get a total of proba to 0, we arrived in a terminal state")
            next_event = first_event
        else:
            dict_of_proba = {x: dict_of_possible_events[x] / total for x in dict_of_possible_events}

            list_keys = list(dict_of_proba.keys())
            next_key = np.random.choice(list(range(len(list_keys))), p=list(dict_of_proba.values()))
            next_event = [list_keys[next_key]]
    except Exception as e:
        print("WARNING We get an event not available in the dictionnary, somethin went wrong")
        print(event)
        next_event = first_event

    return next_event


def get_full_list_events(starting_event, number_of_events, dict_transition_event):
    """
    Build the full list of events from starting event, dictionnary of transition, and the number of events wanted
    """
    final_list_events = copy.deepcopy(starting_event)
    previous_event = copy.deepcopy(starting_event)
    for i in range(number_of_events - 1):
        next_event = predict_next_event(previous_event, dict_transition_event, starting_event)
        final_list_events = final_list_events + next_event
        if len(next_event) == 1:
            previous_event.pop(0)
            previous_event.append(next_event[0])
        else:
            previous_event = copy.deepcopy(next_event)

    return final_list_events
