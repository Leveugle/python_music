import copy

import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage


def split_tracks(midi_file):
    """
    From a midi file, return a list of midi files with only 1 track
    """
    list_midi_files = []
    for i in range(len(midi_file.tracks)):
        temp = MidiFile()
        temp.tracks.append(midi_file.tracks[i])
        list_midi_files.append(temp)
    return list_midi_files


