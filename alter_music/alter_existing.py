import copy

import mido
from mido import Message, MidiFile, MidiTrack
from alter_music.times import change_rythm_music, isolate_time
from alter_music.tracks import split_tracks
from work_on_midi.reading_files import read_files_folder, get_list_of_tracks_from_list_of_midi, \
    get_infos_from_list_of_tracks, get_octaves_count, get_best_two_octaves
from work_on_midi.utils import keep_2_octaves, change_notes_on_2_octaves, get_matching_tracks, clean_list_times
from work_on_midi.track_creation import create_solo_track
from work_on_midi.mixing import concat_n_tracks_in_1, get_n_list_from_unique_list_of_events, concat_n_list_notes_in_1
from work_on_midi.utils import create_track, get_cumulated_time, get_not_cumulated_list_time, \
    get_average_len_of_list_of_notes
from alter_music.notes import compress_same_notes

original = MidiFile("C:\\Users\\sylva\\Documents\\python_music\\music_output\\slower_multi_bramhs.mid", clip=True)


def compress_midi_file(old_path, new_path, velocity):
    """
    Take a midi file, and compress notes when there are 2 following notes
    """
    original = MidiFile(old_path, clip=True)
    new_midi = copy.deepcopy(original)
    for i in range(1, len(original.tracks)):
        list_of_list_octave, list_of_list_notes, list_of_list_time, list_of_instruments = \
            get_infos_from_list_of_tracks([original.tracks[i]])
        list_notes_updated = change_notes_on_2_octaves(list_of_list_notes[0], list_of_list_octave[0])

        # Compress notes
        list_notes_updated, list_times_updated = compress_same_notes(list_notes_updated, list_of_list_time[0])

        track_created = create_track(list_notes_updated, list_times_updated, velocity=velocity,
                                     instrument_number=list_of_instruments[0])
        new_midi.tracks[i] = track_created

    new_midi.save(new_path)


"""compress_midi_file("C:\\Users\\sylva\\Documents\\python_music\\music_output\\slower_multi_bramhs.mid",
                   "C:\\Users\\sylva\\Documents\\python_music\\music_output\\compressed_multi_bramhs.mid",
                   100)
"""


def isolate_time_of_midi(old_path, new_path, starting_time, ending_time, velocity):
    """
    Take a midi file, and compress notes when there are 2 following notes
    By experience, 640 of times = 1s, 38 400 = 1min

    """
    original = MidiFile(old_path, clip=True)
    new_midi = copy.deepcopy(original)
    for i in range(1, len(original.tracks)):
        list_of_list_octave, list_of_list_notes, list_of_list_time, list_of_instruments = \
            get_infos_from_list_of_tracks([original.tracks[i]])
        list_notes_updated = change_notes_on_2_octaves(list_of_list_notes[0], list_of_list_octave[0])

        #isolate_time
        list_notes_updated, list_times_updated = isolate_time(list_notes_updated, list_of_list_time[0], starting_time,
                                                              ending_time)

        track_created = create_track(list_notes_updated, list_times_updated, velocity=velocity,
                                 instrument_number=list_of_instruments[0])
        new_midi.tracks[i] = track_created
    new_midi.save(new_path)


isolate_time_of_midi("C:\\Users\\sylva\\Documents\\python_music\\music_output\\compressed_multi_bramhs.mid",
                     "C:\\Users\\sylva\\Documents\\python_music\\music_output\\multi_bramhs_1min_2min.mid",
                     starting_time=38400,
                     ending_time=76800,
                     velocity=100)
