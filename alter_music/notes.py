import copy
from checks import check_size_lists


def compress_same_notes(list_notes, list_times):
    """
    From a list of notes and a list of times, compress 2 consecuting notes in 1
    """
    check_size_lists([list_notes, list_times])

    new_list_notes = [list_notes[0]]
    new_list_times = [list_times[0]]
    for i in range(1, len(list_notes)):
        # If same notes, we don't add it to the new list but we add the times
        if list_notes[i] == list_notes[i - 1]:
            new_list_times[-1] = new_list_times[-1] + list_times[i]
        else:
            new_list_notes.append(list_notes[i])
            new_list_times.append(list_times[i])
    return new_list_notes, new_list_times
