import copy

import mido
from mido import Message, MidiFile, MidiTrack, MetaMessage
from checks import check_size_lists


def change_rythm_music(midi_file, multiplicator):
    """
    From a midi file, multiply all times by a number to get it slower/faster
    """
    new_midi_file = copy.deepcopy(midi_file)
    for track in new_midi_file.tracks:
        for message in track:
            try:
                message.time = message.time * multiplicator
            except Exception:
                pass
    return new_midi_file


def isolate_time(list_of_notes, list_of_times, starting_time, ending_time):
    """
    Isolate notes and times inside a time interval
    """
    check_size_lists([list_of_notes, list_of_times])
    new_list_times = []
    new_list_notes = []
    cumulated_time = 0
    for i in range(len(list_of_notes)):
        cumulated_time += list_of_times[i]
        if cumulated_time < starting_time:
            continue
        else:
            new_list_times.append(list_of_times[i - 1])
            new_list_notes.append(list_of_notes[i - 1])

        if cumulated_time > ending_time:
            new_list_times.append(list_of_times[i])
            new_list_notes.append(list_of_notes[i])
            break
    return new_list_notes, new_list_times
