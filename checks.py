def check_size_lists(list_of_list):
    """
    On a list of list, check that all lists have same size
    """
    is_ok = True
    if len(list_of_list) > 1:
        first_size = len(list_of_list[0])
        for list in list_of_list:
            if len(list) != first_size:
                is_ok = False
    if not is_ok:
        print("Lists inside list_of_list don't have the same size")
