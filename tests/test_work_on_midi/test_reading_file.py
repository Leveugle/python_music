from work_on_midi.reading_files import get_infos_from_track
import unittest
from mido import Message, MidiFile, MidiTrack, MetaMessage


class TestUtils(unittest.TestCase):

    def test_get_infos_from_track(self):
        # prepare
        track = MidiTrack()

        track.append(MetaMessage('track_name', name='Staff-2', time=0))
        track.append(MetaMessage('instrument_name', name='Piano', time=0))
        track.append(Message('program_change', channel=10, program=1, time=0))
        # Adding useless messages because we need at least 20 messages
        for i in range(15):
            track.append(MetaMessage('instrument_name', name='Piano', time=0))


        track.append(Message('note_on', note=64, velocity=0, time=120))

        track.append(Message('note_on', note=64, velocity=60, time=120))
        track.append(Message('note_on', note=64, velocity=0, time=12))

        track.append(Message('note_on', note=64, velocity=60, time=15))
        track.append(Message('note_on', note=65, velocity=60, time=0))
        track.append(Message('note_on', note=66, velocity=60, time=0))

        track.append(Message('note_on', note=64, velocity=0, time=20))
        track.append(Message('note_on', note=65, velocity=0, time=0))

        track.append(Message('note_on', note=64, velocity=60, time=20))
        track.append(Message('note_on', note=66, velocity=0, time=0))

        track.append(Message('note_on', note=64, velocity=60, time=15))
        track.append(Message('note_on', note=64, velocity=0, time=15))
        track.append(Message('note_on', note=65, velocity=60, time=15))
        track.append(Message('note_on', note=65, velocity=0, time=15))
        track.append(Message('note_on', note=66, velocity=60, time=15))
        track.append(Message('note_on', note=66, velocity=0, time=15))

        expected_list_notes = [4, 4, 4, 4, 5, 6, 4, 5, 4, 6, 4, 4, 5, 5, 6, 6]
        expected_list_times = [120, 120, 12, 15, 0, 0, 20, 0, 20, 0, 15, 15, 15, 15, 15, 15]
        expected_list_octaves = [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]
        expected_instrument = 1

        # act
        res_list_octave, res_list_notes, res_list_time, res_instrument = get_infos_from_track(track)

        # assert
        self.assertEqual(expected_list_notes, res_list_notes)
        self.assertEqual(expected_list_times, res_list_time)
        self.assertEqual(expected_instrument, res_instrument)
        self.assertEqual(expected_list_octaves, res_list_octave)
