from work_on_midi.utils import get_matching_tracks, sort_dict, get_cumulated_time, get_not_cumulated_list_time, \
    get_average_len_of_list_of_notes
from algos.mix_unique_tracks import create_dict_multi_notes
import unittest


class TestUtils(unittest.TestCase):

    def test_upper(self):
        # prepare
        list_notes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9, 7]
        list_times = [4, 4, 0, 0, 4, 0, 4, 4, 0, 0, 0, 3, 0, 4, 0, 4, 0, 0, 0]
        expected = {
            1: {(): 1},
            2: {(3, 4): 1},
            5: {(6,): 2},
            7: {(): 1, (8, 9): 1},
            8: {(1, 2, 9): 1},
            3: {(4,): 1}
        }

        # act
        result = create_dict_multi_notes(list_notes, list_times)

        # assert
        self.assertEqual(expected, result)

    def test_sort_dict(self):
        # prepare
        dictionary = {1: 5, 2: 2}
        expected = {2: 2, 1: 5}
        # act
        result = sort_dict(dictionary)
        # assert
        self.assertEqual(expected, result)

    def test_get_matching_tracks(self):
        # prepare
        dict_midi_files = {
            "music1": {
                "tracks": {
                    1: {
                        "instrument": 1,
                        "octave_minimum": 3,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    2: {
                        "instrument": 41,
                        "octave_minimum": 3,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    3: {
                        "instrument": 1,
                        "octave_minimum": 5,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    }
                }
            },
            "music2": {
                "tracks": {
                    1: {
                        "instrument": 1,
                        "octave_minimum": 3,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    2: {
                        "instrument": 41,
                        "octave_minimum": 7,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    3: {
                        "instrument": 1,
                        "octave_minimum": 6,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    }
                }
            },
            "music3": {
                "tracks": {
                    1: {
                        "instrument": 1,
                        "octave_minimum": 4,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    2: {
                        "instrument": 41,
                        "octave_minimum": 3,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    3: {
                        "instrument": 1,
                        "octave_minimum": 6,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    },
                    4: {
                        "instrument": 1,
                        "octave_minimum": 2,
                        "list_of_notes": [48, 49, 53],
                        "list_of_octaves": [4, 4, 4],
                        "list_of_time": [64, 64, 64]
                    }
                }
            }
        }
        expected = {
            1: {"music1": 1, "music2": 1, "music3": 4},
            2: {"music1": 2, "music2": 2, "music3": 2},
            3: {"music1": 3, "music2": 3, "music3": 1},
        }
        # act
        result = get_matching_tracks(dict_midi_files, main_music_file="music1")

        # assert
        self.assertEqual(expected, result)

    def test_get_cumulated_time(self):
        # prepare
        list_of_times = [120, 120, 12, 15, 0, 0, 20, 0, 20, 0, 15, 15, 15, 15, 15, 15]
        expected = [120, 240, 252, 267, 267, 267, 287, 287, 307, 307, 322, 337, 352, 367, 382, 397]
        # act
        result = get_cumulated_time(list_of_times)
        # assert
        self.assertEqual(expected, result)

    def test_get_not_cumulated_list_time(self):
        # prepare
        list_cumulated_time = [120, 240, 360]

        expected = [120, 120, 120]

        # act
        result = get_not_cumulated_list_time(list_cumulated_time)

        # assert
        self.assertEqual(expected, result)

    def test_get_average_len_of_list_of_notes(self):
        # prepare
        list_notes_1 = [1, 2, 3, 4]
        list_notes_2 = [1, 2, 3, 4, 5, 6]
        list_list_time = [list_notes_1, list_notes_2]

        expected = 5

        # act
        result = get_average_len_of_list_of_notes(list_list_time)

        # assert
        self.assertEqual(expected, result)

    def test_get_average_len_of_list_of_notes_multiple_notes(self):
        # prepare
        list_notes_1 = [(1, 1), (2, 2), (3, 3), (4, 4)]
        list_notes_2 = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6)]
        list_list_time = [list_notes_1, list_notes_2]

        expected = 5

        # act
        result = get_average_len_of_list_of_notes(list_list_time)

        # assert
        self.assertEqual(expected, result)
