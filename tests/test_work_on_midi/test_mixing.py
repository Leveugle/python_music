from work_on_midi.mixing import concat_n_tracks_in_1, concat_n_list_notes_in_1,get_n_list_from_unique_list_of_events

import unittest


class TestUtils(unittest.TestCase):

    def test_concat_n_tracks_in_1(self):
        # prepare

        list_time1 = [120, 240, 252, 267, 267, 267, 287, 292, 307, 307, 322, 337, 352, 367, 382, 397]
        list_note1 = [64, 64, 64, 64, 63, 72, 64, 65, 64, 64, 65, 64, 65, 66, 67, 68]

        list_time2 = [110, 240, 262, 267, 272, 277, 287, 287, 307, 312, 322, 337, 352, 367, 382]
        list_note2 = [65, 65, 65, 65, 64, 73, 65, 66, 65, 65, 66, 65, 66, 67, 68]

        list_time3 = [120, 240, 252, 267, 267, 267, 287, 287, 307, 307, 322, 342, 352, 367, 382, 392, 412]
        list_note3 = [63, 62, 63, 63, 62, 71, 63, 64, 65, 63, 64, 63, 64, 65, 66, 67, 69]
        list_of_list_times = [list_time1, list_time2, list_time3]
        list_of_list_notes = [list_note1, list_note2, list_note3]

        expected_list_times = [110, 120, 240, 252, 262, 267, 272, 277, 287, 292, 307, 312, 322, 337, 342, 352, 367, 382,
                               392, 397, 412]

        expected_list_note1 = [0, 64, 64, 64, 64, [63, 64, 72], [63, 64, 72], [63, 64, 72], 64, 65, 64, 64, 65, 64, 64,
                               65, 66, 67, 67, 68, 68]
        expected_list_note2 = [65, 65, 65, 65, 65, 65, 64, 73, [65, 66], [65, 66], 65, 65, 66, 65, 65, 66, 67, 68, 68,
                               68, 68]
        expected_list_note3 = [0, 63, 62, 63, 63, [62, 63, 71], [62, 63, 71], [62, 63, 71], [63, 64], [63, 64],
                               [63, 65], [63, 65], 64, 64, 63, 64, 65, 66, 67, 67, 69]

        # act
        result_list_times, result_list_of_list_notes = concat_n_tracks_in_1(list_of_list_times, list_of_list_notes)

        # assert
        self.assertEqual(expected_list_times, result_list_times)
        self.assertEqual(expected_list_note1, result_list_of_list_notes[0])
        self.assertEqual(expected_list_note2, result_list_of_list_notes[1])
        self.assertEqual(expected_list_note3, result_list_of_list_notes[2])

    def test_concat_n_list_notes_in_1(self):
        # prepare

        list_note1 = [0,  64, 64, 64, 64, [63, 64, 72], [63, 64, 72], [63, 64, 72],     64,      65,       64,      64,    65, 64, 64, 65, 66, 67, 67, 68, 68]
        list_note2 = [65, 65, 65, 65, 65,      65,           64,            73,     [65, 66], [65, 66],    65,      65,    66, 65, 65, 66, 67, 68, 68, 68, 68]
        list_note3 = [0,  63, 62, 63, 63, [62, 63, 71], [62, 63, 71], [62, 63, 71], [63, 64], [63, 64],[63, 65], [63, 65], 64, 64, 63, 64, 65, 66, 67, 67, 69]
        list_of_list_notes = [list_note1, list_note2, list_note3]

        expected_list_notes = [
            (0,65,0),(64,65,63),(64,65,62),(64,65,63),(64,65,63),(63,65,62),(63,64,62),(63,73,62),
            (64,65,63),(65,65,63),(64,65,63),(64,65,63),(65,66,64),(64,65,64),(64,65,63),(65,66,64),
            (66,67,65),(67,68,66),(67,68,67),(68,68,67),(68,68,69)
        ]


        # act
        result_list_notes = concat_n_list_notes_in_1(list_of_list_notes)

        # assert
        self.assertEqual(expected_list_notes, result_list_notes)


    def test_concat_n_list_notes_in_1_n_equal_1(self):
        # prepare

        list_note1 = [0,  64, 64, 64, 64, [63, 64, 72], [63, 64, 72], [63, 64, 72],     64,      65,       64,      64,    65, 64, 64, 65, 66, 67, 67, 68, 68]
        list_of_list_notes = [list_note1]

        expected_list_notes = [0,  64, 64, 64, 64, 63, 63, 63,     64,      65,       64,      64,    65, 64, 64, 65, 66, 67, 67, 68, 68]


        # act
        result_list_notes = concat_n_list_notes_in_1(list_of_list_notes)

        # assert
        self.assertEqual(expected_list_notes, result_list_notes)


    def test_get_n_list_from_unique_list_of_events(self):
        # prepare


        list_of_events = [
            [0,65,0],[64,65,63],[64,65,62],[64,65,63],[64,65,63],[63,65,62],[63,64,62],[63,73,62],
            [64,65,63],[65,65,63],[64,65,63],[64,65,63],[65,66,64],[64,65,64],[64,65,63],[65,66,64],
            [66,67,65],[67,68,66],[67,68,67],[68,68,67],[68,68,69]
        ]

        list_note1 = [0,  64, 64, 64, 64, 63, 63, 63,     64,      65,       64,      64,    65, 64, 64, 65, 66, 67, 67, 68, 68]
        list_note2 = [65, 65, 65, 65, 65,      65,           64,            73,     65, 65,    65,      65,    66, 65, 65, 66, 67, 68, 68, 68, 68]
        list_note3 = [0,  63, 62, 63, 63, 62, 62, 62, 63, 63,63, 63, 64, 64, 63, 64, 65, 66, 67, 67, 69]
        expected = [list_note1, list_note2, list_note3]

        # act
        result = get_n_list_from_unique_list_of_events(list_of_events)

        # assert
        self.assertEqual(expected, result)
