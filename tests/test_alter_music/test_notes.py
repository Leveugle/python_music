from alter_music.notes import compress_same_notes
import unittest


class TestNotes(unittest.TestCase):

    def test_compress_same_notes(self):
        # prepare
        list_of_times = [1, 10, 20, 50, 100]
        list_of_notes = [4, 4, (4, 5, 6), (4, 5, 6), 8]
        expected_times = [11, 70, 100]
        expected_notes = [4, (4, 5, 6), 8]

        # act
        result_notes, result_times = compress_same_notes(list_of_notes, list_of_times)

        # assert
        self.assertEqual(expected_times, result_times)
        self.assertEqual(expected_notes, result_notes)
