from alter_music.times import isolate_time
import unittest


class TestNotes(unittest.TestCase):

    def test_isolate_time(self):
        # prepare
        list_of_times = [1, 10, 20, 50, 100]
        list_of_notes = [4, 4, (4, 5, 6), (4, 5, 6), 8]
        starting_time = 30
        ending_time = 70
        expected_times = [10, 20, 50]
        expected_notes = [4, (4, 5, 6), (4, 5, 6)]

        # act
        result_notes, result_times = isolate_time(list_of_notes, list_of_times, starting_time, ending_time)

        # assert
        self.assertEqual(expected_times, result_times)
        self.assertEqual(expected_notes, result_notes)
