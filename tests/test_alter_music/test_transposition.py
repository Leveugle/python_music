from alter_music.transposition import transpose, major_to_minor, minor_to_major
import unittest


class TestNotes(unittest.TestCase):

    def test_transpose(self):
        # prepare
        list_of_notes1 = [10, 12, 14, 15, 17, 19, 21, 22]
        list_of_notes2 = [7, 9, 10, 12, 14, 15, 17, 19]

        expected_list_of_notes1 = [7, 9, 10, 12, 14, 15, 17, 19]
        expected_list_of_notes2 = [10, 12, 14, 15, 17, 19, 21, 22]

        # act
        result_list_of_notes1 = transpose(list_of_notes1)
        result_list_of_notes2 = transpose(list_of_notes2)

        # assert
        self.assertEqual(expected_list_of_notes1, result_list_of_notes1)
        self.assertEqual(expected_list_of_notes2, result_list_of_notes2)

    def test_major_to_minor(self):
        # prepare
        list_of_notes1 = [10, 12, 14, 15, 17, 19, 21, 22]

        expected_list_of_notes1 = [7, 9, 10, 12, 14, 15, 17, 19]

        # act
        result_list_of_notes1 = major_to_minor(list_of_notes1, 10)

        # assert
        self.assertEqual(expected_list_of_notes1, result_list_of_notes1)

    def test_minor_to_major(self):
        # prepare
        list_of_notes2 = [7, 9, 10, 12, 14, 15, 17, 19]

        expected_list_of_notes2 = [10, 12, 14, 15, 17, 19, 21, 22]

        # act
        result_list_of_notes2 = minor_to_major(list_of_notes2, 10)

        # assert
        self.assertEqual(expected_list_of_notes2, result_list_of_notes2)
